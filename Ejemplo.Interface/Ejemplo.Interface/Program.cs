﻿using Ejemplo.Interface.Contexto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejemplo.Interface
{
    class Program
    {
        /// <summary>
        /// 
        /// EJEMPLO:
        ///     
        /// Interfaz IID -------                                                     --------- Clase ESTUDIANTE Que hereda Persona
        ///                     ------ Clase Persona que hereda de IID e IPersona ----
        /// Interfaz IPersona---                                                     --------- Clase PROFESOR Que hereda Persona
        /// 
        /// Todo el problema radica en que en c# las clases solo pueden heredar de una y exclusivamente una clase lo cual limita crear clases mas complejas
        /// en base de otras, entonces los de microsoft se idearon en su momento lo que son las interfaces lo cual dice cualquier clase puede heredar de las interfaces que quiera.
        /// Heredar de multiples interfaces(en c#) se llama polimorfismo y es la habilidad que tiene una clase de comportarse como ella misma o como
        /// las clases las cuales hereda.
        /// 
        /// Lo interesante de unas interfaces es que las clases las cuales heredan de una interfaz estan OBLIGADAS SI O SI a implementar las propiedades, metodos, etc que estan en la Interfaz sino 
        /// se te presenta un error de que la clase no hay implementado las propiedades, metodos, ect de X interfaz, miralo como un contrato donde la interfaz le dice a la clase tu puedes heredar
        /// mis miembros(propiedades,metodos,etc) pero solo si lo implementas sino tienes un problema conmigo y no te voy a dejar correr la aplicacion hasta que lo hagas.
        /// 
        /// Interfaces no es mas que un esqueleto de una clase por asi decirlo, no tiene nada implementado solo las propiedades, metodos...a secas, no le puedes hacer un new de una interfaz por ejemplo 
        /// si tienes una interfaz que se llama IPersona, NO LE PUEDES HACER IPersona persona = new IPersona(); ya que solo es un esqueleto.
        /// 
        /// Por lo general las interfaces lo que se quiere es extraer las propiedades, metodos, ect que hay comunes entre las clases e nuestro ejemplo tenemos a estudiantes y profesores, como las clases 
        /// comparten propiedades como nombres, apellidos, edad, determinar si un estudiante es mayor de 30 años para darle un descuento en la matricula o si profesor es menor de 
        /// 30 años para darle un bono de estudio, ect.
        /// 
        /// Eso es lo que realmente tu tratas de sacar en las interfaces miembros comunes entre todas las clases, pero esto realmente le sacas el jugo cuando utilizas genericos,
        /// te pondre un ejemplo para que veas a que me refiero con ello.
        /// 
        /// Normalmente por estandar una interfaz antes de ponerle nombre se le pone la letra I por ejemplo IId, Ipersona.... pero tu la puedes llamar como quieras sino te gusta ese estandar
        /// En este ejemplo tienes 2 Interfaces:
        /// 
        /// IId -> Interfaz que tiene la propiedad entera ID, recuerda que en COdigo primero todas la clases para que se puedan crear como tablas en la base de datos tienen
        /// que tener una propiedad llamada Id.
        /// 
        /// IPersona -> Interfaz que tiene las propiedades comunes que manejan tanto la clase estudiantes como la clase profesor
        /// 
        /// Ahora tengo la Clase PERSONA -> de las cuales heredan las clases EstudianteConInterfaz y ProfesorConInterfaz, el por que de la clase persona no es mas por que voy a utilizar
        /// codigo primero para generar la base de datos y para garantizar por ejemplo que el primer nombre y el primer apellido sea requerido voy a utilizar algo que se llama DATAANOTATION
        /// que no es mas como atributos que le pones encima de la propiedad para indicarle si es requerido, la longitud de la string, o el rango del entero, etc, estos dataanotation cuando 
        /// tienes un textbox con ella en un formulario, sí le pusimos por ejemplo que el primernombre tiene que ser minimo de 2 caracteres al tratar de guardar un primer nombre
        /// con 1 caracter este te muestra un error donde te dice, La restriccion para poder guardar el primer nombre es de minimo de 2 caracteres y usted tiene uno, por favor
        /// corrija y vuelva a intentarlo de nuevo.
        /// 
        /// Ademas de eso, codigo primero permite lo que son archivos de mapeo que es donde por codigo le vas a poner toda la configuracion de la tabla al momento de crearla,
        /// como voy a tener 2 tablas Estudiantes Y Profesores tendria que hacer normalmente 2 archivos de mapeo donde le digo la configuracion una por una de sus campos(que son las propiedades de la clase)
        /// pero con esta configuracion que te estoy mostrando hay que tener 3, Una para PERSONA, ESTUDIANTE Y PROFESOR pero como persona tiene las propiedades en comun practicamente seria hacer un solo archivo de mapeo
        /// ya que tanto estudiante como profesor solo van a quedar como esqueleto, ya que solo alli van a quedar las propiedades diferentes que tienen estudiantes y profesores.
        /// 
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            Ejemplo.Interface.Contexto.Contexto contexto = new Contexto.Contexto();
            var resultado = contexto.Database.CreateIfNotExists();

            if (resultado)            
                Console.WriteLine("Base de datos creada");            
            else
                Console.WriteLine("Base de datos NO creada");
           

            Console.ReadKey();
        }
    }

   
}
