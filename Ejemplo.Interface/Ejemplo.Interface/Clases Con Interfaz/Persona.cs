﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejemplo.Interface.Clases_Con_Interfaz
{
    public class Persona: IId, IPersona
    {
        public int Id { get; set; }

        /// <summary>
        /// Esto son los Data Anotation, son atributos(elementos que se ponen encima de propiedades, metodos, ect) el cual te ayudan a tener mayor control de los datos insertados
        /// aca le estoy diciendo que el primer nombre es requerido, que el minimo y maximo de caracteres aceptado van a ser 2 y 50 respectivamente y ademas en caso de que no se
        /// cumpla esto muestre los mensajes que cada uno de ellos tiene, si quieres investiga mas sobre ellos que no son muchos y son facil de utilizar.
        /// 
        /// Estos Data Anotations son mas que todos utilizados para la parte visual, existes unos en los archivos de mapeo que son exclusivos para la configuracion de la tabla persona
        /// </summary>
        [Required]
        [MinLength(2, ErrorMessage ="El Primer Nombre tiene que ser de minimo de 2 caracteres")]
        [MaxLength(50, ErrorMessage = "El Primer Nombre tiene que ser de maximo de 50 caracteres")]
        public String PrimerNombre { get; set; }


        /// <summary>
        /// Segundo nombre como no es requerido no le pongo nada, aca se puede aceptar tanto null como espacios en blanco
        /// </summary>
        public String SegundoNombre { get; set; }

        [MinLength(2, ErrorMessage = "El Primer Nombre tiene que ser de minimo de 2 caracteres")]
        [MaxLength(50, ErrorMessage = "El Primer Nombre tiene que ser de maximo de 50 caracteres")]
        public String PrimerApellido { get; set; }

        [MinLength(2, ErrorMessage = "El Primer Nombre tiene que ser de minimo de 2 caracteres")]
        [MaxLength(50, ErrorMessage = "El Primer Nombre tiene que ser de maximo de 50 caracteres")]
        public String SegundoApellido { get; set; }


        /// <summary>
        /// Indico que la edad tiene que estar entre 1 y 150 años
        /// </summary>
        [Range(1, 150, ErrorMessage = "Edad Invalida")]
        public int Edad { get; set; }


        /// <summary>
        /// Este metodo se va a sobreescribir tanto en la clase de estudiante como en profesor, ya que cada uno de ellos lo va a utilizar pero de forma difente
        /// </summary>
        /// <param name="edad"></param>
        /// <returns></returns>
        public virtual bool DeterminarSiLaPersonaEsMayorDe30Anios(int edad)
        {
            throw new NotImplementedException();
        }
    }
}
