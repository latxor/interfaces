﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejemplo.Interface.Clases_Con_Interfaz
{
    /// <summary>
    /// Debido a que heredas de persona ya tienes las propiedades primernombre.... aca en esta clase, si le das this. dentro de un metodo veras que te aparecen pero
    /// por lo tanto ya no tienes que ponerlas aca, aca solo vas a colocoar las propiedades que hacen unica a la clase estudiantes por ejemplo ValorMatricula, tambien puede ser una Clase HOrario, una propiedad Acudiente, 
    /// propiedades solo que aplican a un estudiante.
    /// 
    /// En nuestro caso tengo Valor de la matricula, si ves tengo el metodo DeterminarSiLaPersonaEsMayorDe30Anios que sale de la interfaz IPersona. Tanto Estudiante y Profesor
    /// lo heredan de esa interfaz pero cada uno tiene una implementacion diferente para el caso del estudiante yo le hago un descuento en la matricula si la persona es mayor de 30 años
    /// y en profesor le aumento el sueldo.
    /// 
    /// Si ves tengo un atributo de Data Anotation que es[NotMapped], esto se utiliza para propiedades que no quieres que aparezcan en la base de datos pero que aun asi las neceseitas en la clase
    /// con esto te evitas que codigo primero coja la propiedad ValorMatricula y la meta en la tabla estudiante de tu base de datos como un campo mas.
    /// </summary>
    public class Estudiante : Persona
    {
        [NotMapped]
        public double ValorMatricula { get; set; }        

        public override bool DeterminarSiLaPersonaEsMayorDe30Anios(int edad)
        {
            if (this.Edad > 30)
            {
                this.ValorMatricula = ValorMatricula * 0.70;
                return true;
            }
            else
            {
                this.ValorMatricula = ValorMatricula * 0.50;
                return false;
            }
        }
    }
}
