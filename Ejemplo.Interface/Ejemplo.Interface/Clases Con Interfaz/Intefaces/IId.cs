﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejemplo.Interface.Clases_Con_Interfaz
{
    interface IId
    {
        /// <summary>
        /// Esqueleto de la Propiedad Id
        /// </summary>
        int Id { get; set; }
       
    }
}
