﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejemplo.Interface.Clases_Con_Interfaz
{
    interface IPersona
    {
        /// <summary>
        /// Esqueleto de la Propiedad Primer Nombre
        /// </summary>
        String PrimerNombre { get; set; }

        /// <summary>
        /// Esqueleto de la Propiedad Nombre
        /// </summary>
        String SegundoNombre { get; set; }


        /// <summary>
        /// Esqueleto de la Propiedad Apellido
        /// </summary>
        String PrimerApellido { get; set; }

        /// <summary>
        /// Esqueleto de la Propiedad Apellido
        /// </summary>
        String SegundoApellido { get; set; }

        /// <summary>
        /// Esqueleto de la Propiedad Edad
        /// </summary>
        int Edad { get; set; }

        /// <summary>
        /// Esqueleto del metodo que regresa verdadero si una persona es adulta o falso si no lo es.
        /// </summary>
        /// <param name="edad"></param>
        /// <returns></returns>
         bool DeterminarSiLaPersonaEsMayorDe30Anios(int edad);
    }
}
