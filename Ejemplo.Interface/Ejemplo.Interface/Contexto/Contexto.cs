﻿using Ejemplo.Interface.Clases_Con_Interfaz;
using Ejemplo.Interface.Contexto.Mapeos;

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejemplo.Interface.Contexto
{
    public class Contexto : DbContext
    {
        /// <summary>
        /// Cambiar cadena de coneccion por la tuya
        /// </summary>
        public Contexto() : base(@"Server=.\DESARROLLO;Database=EstudianteYProfesor;User Id=sa;Password=sapassword;")
        {
        }
        /// <summary>
        /// Aca se indican cuales son las tablas que van a ser parte de la base de datos, mira que a pesar de que Persona tiene su archivo de mapeo no se va a crear ninguna
        /// tabla con ella, ya que solo cumple el objectivo de centralizar las propiedades comunes de las clases estudiantes y profesor que son realmente nos intersa guardar
        /// en la base de datos
        /// </summary>
        public DbSet<Estudiante> Estudiante { get; set; }
        public DbSet<Profesor> Profesor { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //ACA ES DONDE LLAMAMOS LOS ARCHIVOS DE MAPEO, que es lo que contiene la configuracion de cada tabla
            modelBuilder.Configurations.Add(new EstudianteMapeo());
            modelBuilder.Configurations.Add(new ProfesorMapeo());
            base.OnModelCreating(modelBuilder);
        }
     

     
    }
}