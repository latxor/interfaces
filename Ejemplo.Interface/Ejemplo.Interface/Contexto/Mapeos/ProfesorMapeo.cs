﻿using Ejemplo.Interface.Clases_Con_Interfaz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejemplo.Interface.Contexto.Mapeos
{
    /// <summary>
    /// Debido a que persona tiene las propiedades comunes entre estudiantes y profesores y ya persona tiene su archivo de mapeo es solo heredar de ese clase (personaMapeo) y
    /// al heredar el se trae toda la configuracion que tenemos alla, al traerse aca esa configuracion ya no tenemos que hacer nada. Sí no tuviemos la clase persona tendrias que hacer la configuracion
    /// tanto de estudiante como de profesor es decir en la clase de estudiante y profesor tendrias que configurar primernombre, segundonombre... ect lo que estarias practicamente
    /// duplicando codigo que es lo que se quiere evitar a toda costa por que ahora aca son 2 clases, pero imaginate que fuesen 10 clases y que fuesen 10 propiedades comunes,
    /// el tiempo que tendrias que gastarle es mucho
    /// 
    /// 
    /// Al heredar de persona practicamente le estamos diciendo ves al archivo PersonaMapeo y traeme la configuracion de las propiedades acá(asi sea que no lo veas el ya tiene la configuracion de PersonaMapeo aca) es decir
    /// PrimerNOmbre,segundonombre, ect ya estan aca y en esta clase solo le vamos a decir como se llama la tabla ToTable("Profesor");
    /// </summary>
    public class ProfesorMapeo: PersonaMapeo<Profesor> //Recuerda que Persona Mapeo es generica por lo tanto tienes que darle una clase que implemente persona, trata de quitarle el <> para que veas que te presenta un error.
    {
        public ProfesorMapeo()
        {            
            //Con esta configuracion le estamos diciendo el nombre de como se llama la tabla
            ToTable("Profesor");
        }
    }
}
