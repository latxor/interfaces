﻿using Ejemplo.Interface.Clases_Con_Interfaz;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejemplo.Interface.Contexto.Mapeos
{

    /// <summary>
    /// ESTOS SON LOS FAMOSOS GENERICOS, tratare de explicarte de forma sencilla para que le captes de que va la cosa.
    /// Acá lo que estoy diciendo es cualquiera clase que quiera heredar de PersonaMapeo o utilizar PersonaMapero tiene que OBLIGATORIAMENTE heredar de la clase Persona, 
    /// en nuestro clase tenemos Estudiante y Profesor que ya heredan de Persona:
    /// Por ejemplo si quiesiera utilizar PersonaMapeo en otra clase seria algo como: 
    /// 
    /// PersonaMapeo<Estudiante> = new PersonaMapeo<Estudiante>();
    /// PersonaMapeo<Profesor> = new PersonaMapeo<Profesor>();
    /// 
    /// o si la quieren heredar seria algo como
    /// 
    /// public class OtraClase: PersonaMapeo<Estudiante>{}
    /// 
    /// OJO-> lo realmemente importante aquí es que la clase que pongas dentro de los simbolos de <> implemente la clase persona ya que eso es lo que hace la funcion where que ves abajo,
    /// si miras estamos diciendo "where CualquierClaseQueImplementeLaClasePersona : Persona", cualquier clase que me quiera utilizar si o si tiene que tener persona en alguna parte de ella
    /// sino no te voy a permitir utilizarme.
    /// 
    /// CualquierClaseQueImplementeLaClasePersona no es mas que un nombre generico que le pongo acá ya que todavia no se que tipo clase me van a pasar, puedes llamarlo T como 
    /// normalmente se le llama o como tu quieras, vas a tener la certeza de que la clase que le metan en <> va a tener los metodos, propiedades, etc de Persona.
    /// En nuestro caso al momento de decir PersonaMapeo<Estudiante> = new PersonaMapeo<Estudiante>(); ese CualquierClaseQueImplementeLaClasePersona se convierte automanticamente en
    /// el tipo de clase Persona ya que estudiante la hereda.
    /// </summary>
    /// <typeparam name="TModelo"></typeparam>
    public class PersonaMapeo<CualquierClaseQueImplementeLaClasePersona> : EntityTypeConfiguration<CualquierClaseQueImplementeLaClasePersona>
                where CualquierClaseQueImplementeLaClasePersona : Persona
    {
        //OJO LA CONFIGURACIÓN DE LA TABLA SIEMPRE VA EN EL CONTRUCTOR POR DEFECTO
        public PersonaMapeo()
        {
            
            
            //Aca indico que tengo la primary key(llame primaria) va a ser la propiedad Id
            HasKey(t => t.Id);
            //Que el campo en la tabla se va a llamar Id
            Property(t => t.Id).HasColumnName("Id");
            //Y que va a ir sumando de 1 en 1 cada ves que se inserte una fila en la tabla
            Property(t => t.Id).HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);

            //Primer Nombre -> Indico que va a ver un campo en la tabla que se va aa llamar PrimerNombre, 
            //que maximo va a ser de 50 caracters y que va a ser requerido, osea que no va aceptar null 
            Property(t => t.PrimerNombre).HasColumnName("PrimerNombre").HasMaxLength(50).IsRequired();

            //Segundo Nombre -> Solo de indico que el campo se va a llamar SegundoApellido y que va ser opcional osea va aceptar null 
            Property(t => t.SegundoApellido).HasColumnName("SegundoApellido").IsOptional();


            //Así sigo configurando cada una de las propiedades que tengo en la clase persona....

            Property(t => t.PrimerApellido).HasColumnName("PrimerApellido").HasMaxLength(50).IsRequired();

            Property(t => t.SegundoApellido).HasColumnName("SegundoApellido").HasMaxLength(50).IsRequired();

            Property(t => t.Edad).HasColumnName("Edad").IsRequired();

        }
    }
}
