﻿using Ejemplo.Interface.Clases_Con_Interfaz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejemplo.Interface.Contexto.Mapeos
{

    /// <summary>
    /// Leer la clase ProfesorMapeo primero.....
    /// 
    ///  Al heredar de personaMapeo practicamente le estamos diciendo ves al archivo PersonaMapeo y traeme la configuracion de las propiedades acá(asi sea que no lo veas el ya tiene la configuracion de PersonaMapeo aca) es decir
    /// PrimerNOmbre,segundonombre, ect ya estan aca y en esta clase solo le vamos a decir como se llama la tabla ToTable("Estudiante");
    /// </summary>
    public class EstudianteMapeo: PersonaMapeo<Estudiante>
    {
        public EstudianteMapeo()
        {
            ToTable("Estudiante");
        }
    }
}
